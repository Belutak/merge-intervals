package com.merge;

import java.util.*;

public class Main {

    //flag will remain false if condition for merging is never fulfilled, thus -1 is printed
    private static boolean flag = false;

    public static void main(String[] args) {

        List<Interval> intervals = new ArrayList<>();
        //test overlapping case
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(8, 10));
        intervals.add(new Interval(15, 18));
        //test no overlapping case
//        intervals.add(new Interval(1, 3));
//        intervals.add(new Interval(5, 7));
//        intervals.add(new Interval(8, 10));

        //new array list that will hold sorted list
        List<Interval> finished;
        finished = Solution.merge(intervals);

        //if there was overlapping print list of intervals
        int i = 0;
        if (flag) {
            for (Interval temp : finished) {
                System.out.print("[" + temp.getStart() + "," + temp.getEnd() + "]");
                i++;
                if (i < finished.size()) {
                    System.out.print(", ");
                }
            }
        } else
            System.out.println(-1);
    }

    public static class Solution {
        //method to sort passed list
        public static List<Interval> merge(List<Interval> intervals) {
            //lambda expression instead of creating comparator
            intervals.sort(Comparator.comparingInt(a -> a.start));
            //list to hold new merged array
            List<Interval> merged = new ArrayList<>();
            Interval interval = intervals.get(0);
            //copy/merge passed list to a new list
            for (int i = 1; i < intervals.size(); i++) {
                Interval current = intervals.get(i);
                //if there is overlap/merging
                if (interval.getEnd() >= current.getStart()) {
                    interval = new Interval(interval.getStart(), Math.max(interval.getEnd(), current.getEnd()));
                    flag = true;
                } else {
                    merged.add(interval);
                    interval = current;
                }
            }
            merged.add(interval);

            return merged;
        }
    }

    //class that will represent one element of a list
    public static class Interval {
        private int start;
        private int end;

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public void setEnd(int end) {
            this.end = end;
        }

        @Override
        public String toString() {
            return start + ", " +
                    end;
        }
    }
}


